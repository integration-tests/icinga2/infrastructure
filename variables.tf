
# required_version = ">= 0.12"

# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592

variable "servers" {

  type = map(map(string))

  default = {
    "database"= {
        memory    = 512
        vcpu      = 1
        disk_size = "8589934592"
        octetIP   = 10
        hostname  = "database-1"
    }

    "icingaweb" = {
        memory    = 512
        vcpu      = 1
        disk_size = "8589934592"
        octetIP   = 15
        hostname  = "icingaweb"
    }

    "icinga_master_1" = {
        memory    = 512
        vcpu      = 1
        disk_size = "8589934592"
        octetIP   = 20
        hostname  = "master-2"
    }
  }
}
