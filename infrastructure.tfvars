
# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592
# Size in bytes (4 GB)
# size = 4294967296

servers = {
  "database" = {
    "memory"    = 2048
    "vcpu"      = 1
    "disk_size" = "6442450944"
    octetIP     = 10
    "hostname"  = "database"
  }
  "tsdb" = {
    "memory"    = 2048
    "vcpu"      = 1
    "disk_size" = "6442450944"
    octetIP     = 12
    "hostname"  = "tsdb"
  }
  "icingaweb" = {
    "memory"    = 2048
    "vcpu"      = 1
    "disk_size" = "6442450944"
    octetIP     = 15
    "hostname"  = "icingaweb"
  }

  "master-1" = {
    "memory"    = 2048
    "vcpu"      = 1
    "disk_size" = "6442450944"
    octetIP     = 20
    "hostname"  = "master-1"
  }

  "master-2" = {
    "memory"    = 2048
    "vcpu"      = 1
    "disk_size" = "4294967296"
    octetIP     = 21
    "hostname"  = "master-2"
  }

  "satellite-1" = {
    "memory"    = 1024
    "vcpu"      = 1
    "disk_size" = "4294967296"
    octetIP     = 30
    "hostname"  = "satellite-1"
  }

  "satellite-2" = {
    "memory"    = 1024
    "vcpu"      = 1
    "disk_size" = "4294967296"
    octetIP     = 31
    "hostname"  = "satellite-2"
  }
}
