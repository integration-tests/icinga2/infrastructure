
resource "libvirt_network" "vm_network" {
  name      = "icinga_network"
  domain    = "icinga.local"
  addresses = ["192.168.130.0/24"]
  dns {
    enabled    = true
    local_only = true
  }
}
